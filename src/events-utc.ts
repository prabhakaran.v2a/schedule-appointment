
import moment from "moment";
let baseData: {
    RoomID: any;
    TaskID: any; OwnerID: any; Title: string; Description: string; StartTimezone: null; Start: string; End: string; EndTimezone: null; RecurrenceRule: null;
    RecurrenceID: null; RecurrenceException: null; isAllDay: boolean;
}[] = [];
let sample = JSON.parse(localStorage.getItem("bookList") || "");
sample?.map((data: any, key: any)=>{
    let date = moment(data.date).format("YYYY-MM-DD");
    let start = data.slot.split("-")[0];
    let end = data.slot.split("-")[1];
    let updateSlot = {
        TaskID: key,
        OwnerID: key,
        Title: `${data.name} - ${data.consult}`,
        Description: `${data.consult}`,
        StartTimezone: null,
        Start: `${date}T${start}:00.000Z`,
        End: `${date}T${end}:00.000Z`,
        EndTimezone: null,
        RecurrenceRule: null,
        RecurrenceID: null,
        RecurrenceException: null,
        isAllDay: false,
        RoomID: null
    }
    baseData.push(updateSlot);
})

export const customModelFields = {
    id: 'TaskID',
    title: 'Title',
    description: 'Description',
    start: 'Start',
    end: 'End',
    recurrenceRule: 'RecurrenceRule',
    recurrenceId: 'RecurrenceID',
    recurrenceExceptions: 'RecurrenceException'
};

const currentYear = new Date().getFullYear();
const parseAdjust = (eventDate: string | number | Date) => {
    const date = new Date(eventDate);
    date.setFullYear(currentYear);
    return date;
};

const randomInt = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1)) + min;

export const displayDate = new Date(Date.UTC(currentYear, 5, 24));

export const sampleData = baseData.map(dataItem => (
    {
        id: dataItem.TaskID,
        start: parseAdjust(dataItem.Start),
        startTimezone: dataItem.StartTimezone,
        end: parseAdjust(dataItem.End),
        endTimezone: dataItem.EndTimezone,
        isAllDay: dataItem.isAllDay,
        title: dataItem.Title,
        description: dataItem.Description,
        recurrenceRule: dataItem.RecurrenceRule,
        recurrenceId: dataItem.RecurrenceID,
        recurrenceExceptions: dataItem.RecurrenceException,

        roomId: dataItem.RoomID,
        ownerID: dataItem.OwnerID,
        personId: dataItem.OwnerID
    }
));

export const sampleDataWithResources = baseData.map(dataItem => (
    {
        id: dataItem.TaskID,
        start: parseAdjust(dataItem.Start),
        startTimezone: dataItem.StartTimezone,
        end: parseAdjust(dataItem.End),
        endTimezone: dataItem.EndTimezone,
        isAllDay: dataItem.isAllDay,
        title: dataItem.Title,
        description: dataItem.Description,
        recurrenceRule: dataItem.RecurrenceRule,
        recurrenceId: dataItem.RecurrenceID,
        recurrenceExceptions: dataItem.RecurrenceException,
        roomId: randomInt(1, 2),
        personId: randomInt(1, 2)
    }
));

export const sampleDataWithCustomSchema = baseData.map(dataItem => (
    {
        ...dataItem,
        Start: parseAdjust(dataItem.Start),
        End: parseAdjust(dataItem.End),
        PersonIDs: randomInt(1, 2),
        RoomID: randomInt(1, 2)
    }
));
