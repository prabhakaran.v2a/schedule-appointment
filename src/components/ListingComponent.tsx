import React, { useEffect, useState } from 'react'
import * as ReactDOM from "react-dom";
import { MdOutlineCalendarMonth, MdPlaylistAdd } from "react-icons/md";
import {FaUser} from "react-icons/fa";
import moment from 'moment';
import Moment from 'react-moment';
import { Form, Steps, TimePicker, DatePicker, Button, Alert, Input } from 'antd';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat'
import {
    Scheduler,
    WeekView,
    MonthView,

    SchedulerSlot,
    SchedulerSlotProps
} from '@progress/kendo-react-scheduler';
import Calendar from './Calendar';
dayjs.extend(customParseFormat)
const { RangePicker } = TimePicker;

const ListingComponent = () => {
    let time = new Date();
    let date = new Date();
    const [tab, setTab] = useState("list");
    const [create, setCreate] = useState(false);
    const [bookList, setBookList] = useState<any>([]);
    const [status, setStatus] = useState<any>("");
    const [current, setCurrent] = useState<any>(0);
    const [value, setValue] = useState<any>(null);
    const [confirmSlot, setConfirmSlot] = useState<any>("");
    let defaultList = [
        {
            "name": "Arman",
            "slot": "04:00-04:30",
            "date": new Date(),
            "consult": "ENT",
            "confirmation": "booked",
        },
        {
            "name": "Rahul",
            "slot": "04:30-05:00",
            "date": new Date(),
            "consult": "ENT",
            "confirmation": "booked",
        },
        {
            "name": "Janu",
            "slot": "05:00-05:30",
            "date": new Date(),
            "consult": "ENT",
            "confirmation": "booked",
        },
        {
            "name": "Kumar",
            "slot": "05:30-06:00",
            "date": new Date(),
            "consult": "ENT",
            "confirmation": "booked",
        },
        {
            "name": "Arun",
            "slot": "06:00-06:30",
            "date": new Date(),
            "consult": "ENT",
            "confirmation": "booked",
        }
    ]
    useEffect(()=>{
        localStorage.setItem("bookList",JSON.stringify(defaultList));
        setBookList(defaultList);
    },[])
    useEffect(() => {
        setCurrent(0);
        setConfirmSlot("");
        setStatus("");
        viewTitle();
        if(tab === "list"){
            localStorage.getItem("bookList")
        }
    }, [tab, create])
    const viewTitle = () => {
        if(create){
            return "Book an Appointment";
        } else if(tab === "list"){
            return "List of Appointments"
        } else if( tab === "calendar") {
            return "Calendar View Mode"
        }
    }
    const CreatComponent = () => {
        const items = [
          {
            title: 'Check Slot',
          },
          {
            title: 'Provide Details',
          },
          {
            title: 'Confirmation',
          },
        ];
        const onChange = (time: any, timeString: string) => {

            console.log(timeString);
        };
        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 16 },
            },
          };
        const rangeConfig = {
            rules: [{ type: 'array' as const, required: true, message: 'Please select time!' }],
        };
        const onFinish = (fieldsValue: any) => {
        // Should format date value before submit.
        const rangeTimeValue = fieldsValue['range-time-picker'];
        const values = {
            ...fieldsValue,
            'range-time-picker': [
            rangeTimeValue[0].format('YYYY-MM-DD HH:mm:ss'),
            rangeTimeValue[1].format('YYYY-MM-DD HH:mm:ss'),
            ],
        };
        let slotRange = moment(values["range-time-picker"][0]).format("HH:mm") + "-" + moment(values["range-time-picker"][1]).format("HH:mm");
        checkSlotAvaiable(slotRange);
        };
        const onConfirm = (fieldsValue: any) => {
            let payload = {
                "name": fieldsValue.name,
                "slot": confirmSlot,
                "date": new Date(),
                "consult": fieldsValue.medical,
                "confirmation": "booked",
            }
            setBookList([...bookList, payload]);
            localStorage.setItem("bookList",JSON.stringify([...bookList, payload]));
            setCurrent(2);
            };
        const checkSlotAvaiable = (slot:any) => {
            let bookSlot = slot
            let bookStart = bookSlot.split("-")[0];
            let bookEnd = bookSlot.split("-")[1];
            bookList.forEach((val:any) => {
                let start = val.slot.split("-")[0];
                let end = val.slot.split("-")[1];
                console.log(slot,val.slot)
                if(bookStart >= end && bookEnd >= end){
                    setStatus("Slot Available");
                    setConfirmSlot(slot);
                    setCurrent(1);
                } else if(start <= bookStart && end >= bookEnd){
                    setStatus("Slot Overlap");
                } else {
                    setStatus("Slot Not Available")
                }
            })
            
            
        }   
        
        return (
            <>
                <Steps current={current} labelPlacement="vertical" items={items} />
                {current === 0 && (
                    <div className=" mt-12 p-[4rem] bottom-[2rem] border-solid border-slate-400 rounded-md shadow"> 
                        <Form
                            name="time_related_controls"
                            {...formItemLayout}
                            onFinish={onFinish}
                            style={{ maxWidth: 600 }}
                        >
                            <Form.Item name="range-time-picker" label="Select Slot" {...rangeConfig}>
                                <RangePicker format="HH:mm"/>
                            </Form.Item>
                            <Form.Item wrapperCol={{ xs: { span: 24, offset: 0 }, sm: { span: 16, offset: 8 } }}>
                                <Button type="primary" htmlType="submit">
                                    Check Slot
                                </Button>
                            </Form.Item>
                            
                        </Form>
                        {status && 
                                <div className="mt-[4rem] bottom-2 border-slate-400 rounded-md shadow-md">
                                    <Alert message={status} type="error" />
                                </div>
                            }
                    </div>
                )}
                {current === 1 && (
                    <div className=" h-[40vh] bottom-2 border-slate-400 rounded-md shadow-md">
                        <div className="mx-5 pt-[2rem]">
                            <Alert message={"Slot Avialable : " + confirmSlot} type="success" />
                        </div>
                        <div className="mt-3">
                        <Form
                            name="book_appointment"
                            {...formItemLayout}
                            layout={"horizontal"}
                            onFinish={onConfirm}
                            style={{ maxWidth: 600 }}
                            >
                            <Form.Item name="name" label="Patient Name">
                                <Input />
                            </Form.Item>
                            <Form.Item name="medical" label="Medical Cabinets">
                                <Input />
                            </Form.Item>
                            <Form.Item >
                                <Button type="primary" htmlType="submit">Book</Button>
                            </Form.Item>
                        </Form>
                        </div>
                    </div>
                )}
                {current === 2 && (
                    <div className=" h-[40vh] bottom-2 border-slate-400 rounded-md shadow-md">
                        <div className="mx-5 pt-[2rem]">
                            <Alert message={"Booking Successfull"} type="success" />
                        </div>
                        <div onClick={()=> { setTab("list"); setCreate(false) }}>
                            <Button className="mt-4" type="primary">Confirm</Button>
                        </div>
                        
                    </div>
                )}
            </>
        )
    }
    const CustomSlot = (props: SchedulerSlotProps) => (
        <SchedulerSlot
          {...props}
          style={{
                  ...props.style,
                  borderBottom: props.isAllDay ? '1px solid red' : undefined
              }}
          />)
    return (
        <>
        <nav className="fixed top-0 z-50 w-full bg-white border-b border-gray-200 dark:bg-gray-800 dark:border-gray-700">
            <div className="px-3 py-3 lg:px-5 lg:pl-3">
                <div className="flex items-center justify-between">
                    <div className="flex items-center justify-start">
                        <button data-drawer-target="logo-sidebar" data-drawer-toggle="logo-sidebar" aria-controls="logo-sidebar" type="button" className="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600">
                            <span className="sr-only">Open sidebar</span>
                            <svg className="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path clip-rule="evenodd" fill-rule="evenodd" d="M2 4.75A.75.75 0 012.75 4h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 4.75zm0 10.5a.75.75 0 01.75-.75h7.5a.75.75 0 010 1.5h-7.5a.75.75 0 01-.75-.75zM2 10a.75.75 0 01.75-.75h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 10z"></path>
                            </svg>
                        </button>
                        
                        <a href="#" className="flex ml-2 md:mr-24">
                            <img src="https://flowbite.com/docs/images/logo.svg" className="h-8 mr-3" alt="FlowBite Logo" />
                            <span className="self-center text-xl font-semibold sm:text-2xl whitespace-nowrap dark:text-white">10BedICU Appointment</span>
                        </a>
                    </div>
                </div>
            </div>
        </nav>
        <aside id="logo-sidebar" className="fixed top-0 left-0 z-40 w-64 h-screen pt-20 transition-transform -translate-x-full bg-white border-r border-gray-200 sm:translate-x-0 dark:bg-gray-800 dark:border-gray-700" aria-label="Sidebar">
            <div className="h-full px-3 pb-4 overflow-y-auto bg-white dark:bg-gray-800">
                <ul className="space-y-2 font-medium cursor-pointer">
                    <li>
                        <div className="flex gap-5 items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700" onClick={()=> { setTab("list"); setCreate(false) }}>
                            <FaUser />
                            <span className="">View Appoiments</span>
                        </div>
                    </li>
                    <li>
                        <div className="flex gap-5 items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700" onClick={()=> { setTab("calendar"); setCreate(false)}}>
                            <MdOutlineCalendarMonth size={20}/>
                            <span className="">Calendar</span>
                        </div>
                    </li>
                </ul>
            </div>
        </aside>
        <div className="px-6 space-y-2 sm:ml-64 mt-[5.5rem]">
            <div className="flex  justify-between pb-4 bg-white dark:bg-gray-900">
                <label htmlFor="table-search" className="sr-only">Search</label>
                <div className="relative">
                    {viewTitle()}
                </div>
                <div onClick={() => setCreate(!create)}>
                    <button className="inline-flex cursor-pointer space-x-2 items-center text-gray-500 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-3 py-1.5 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700" type="button">
                        <MdPlaylistAdd size={20}/>
                        <span>New Appointment</span>
                    </button>
                </div>
            </div>
            {tab === "list" && (
                <>
                    {create ? (
                        <div>
                            <CreatComponent/>
                        </div>
                    )   : (
                        
                        
                        <div className="relative overflow-x-auto shadow-md sm:rounded-lg ">
                            <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                                <thead className="text-sm h-16 text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                    <tr>
                                        <th scope="col" className="px-6 py-3">
                                            Time Slot
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Date
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Patient
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Medical Cabinets
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Appointment
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {bookList?.map((data: any) => (
                                        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                            <td className="px-6 py-4">
                                                {data.slot}
                                            </td>
                                            <td className="px-6 py-4">
                                                <Moment format="DD/MM/YYYY">{data.date}</Moment>
                                            </td>
                                            <td className="px-6 py-4">
                                                {data.name}
                                            </td>
                                            <td className="px-6 py-4">
                                                {data.consult}
                                            </td>
                                            <td className="px-6 py-4">
                                                <div className="bg-green-200 font-semibold w-min px-2 rounded-md border-solid border-1 ">
                                                    {data.confirmation}
                                                </div>
                                                
                                            </td>
                                        </tr>
                                    ))}
                                        
                                    
                                </tbody>
                            </table>
                        </div>
                    )}
                </>
            )}
            {tab === "calendar" && (
                <>
                {create ? (
                    <div>
                    <CreatComponent/>
                    </div>
                ) : (
                    <Calendar/>
                )}
            </>
               
            )}
        </div>
        
            </>
    )
}

export default ListingComponent


function globalizeLocalizer(globalize: any) {
    throw new Error('Function not implemented.');
}

