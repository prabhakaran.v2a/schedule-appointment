import React, { useEffect, useState } from "react";
import {
  Scheduler,
  TimelineView,
  WeekView,
  DayView,
  SchedulerViewSlot,
  SchedulerViewSlotProps,
} from "@progress/kendo-react-scheduler";
import { sampleData, displayDate } from "../events-utc";
import moment from "moment";

const Calendar = () => {
    let [baseData, setBaseData] = useState<any>([]);
    useEffect(() => {
        let sample = JSON.parse(localStorage.getItem("bookList") || "");
        let newSample = sample?.map((data: any, key: any)=>{
            let date = moment(data.date).format("YYYY-MM-DD");
            let start = data.slot.split("-")[0];
            let end = data.slot.split("-")[1];
            let updateSlot = {
               TaskID: key,
        OwnerID: key,
        Title: `${data.name} - ${data.consult}`,
        Description: `${data.consult}`,
        StartTimezone: null,
        Start: `${date}T${start}:00.000Z`,
        End: `${date}T${end}:00.000Z`,
        EndTimezone: null,
        RecurrenceRule: null,
        RecurrenceID: null,
        RecurrenceException: null,
        isAllDay: false,
        RoomID: null
            }
            return updateSlot;
        })
        setBaseData(newSample);
    },[])

    const customModelFields = {
        id: 'TaskID',
        title: 'Title',
        description: 'Description',
        start: 'Start',
        end: 'End',
        recurrenceRule: 'RecurrenceRule',
        recurrenceId: 'RecurrenceID',
        recurrenceExceptions: 'RecurrenceException'
    };
    
    const currentYear = new Date().getFullYear();
    const parseAdjust = (eventDate: string | number | Date) => {
        const date = new Date(eventDate);
        date.setFullYear(currentYear);
        return date;
    };
    
    const randomInt = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1)) + min;
    
    const displayDate = new Date(Date.UTC(currentYear, 5, 24));
    
    const sampleData = baseData.map((dataItem: { TaskID: any; Start: string | number | Date; StartTimezone: any; End: string | number | Date; EndTimezone: any; isAllDay: any; Title: any; Description: any; RecurrenceRule: any; RecurrenceID: any; RecurrenceException: any; RoomID: any; OwnerID: any; }) => (
        {
            id: dataItem.TaskID,
            start: parseAdjust(dataItem.Start),
            startTimezone: dataItem.StartTimezone,
            end: parseAdjust(dataItem.End),
            endTimezone: dataItem.EndTimezone,
            isAllDay: dataItem.isAllDay,
            title: dataItem.Title,
            description: dataItem.Description,
            recurrenceRule: dataItem.RecurrenceRule,
            recurrenceId: dataItem.RecurrenceID,
            recurrenceExceptions: dataItem.RecurrenceException,
    
            roomId: dataItem.RoomID,
            ownerID: dataItem.OwnerID,
            personId: dataItem.OwnerID
        }
    ));
    
    const sampleDataWithResources = baseData.map((dataItem: { TaskID: any; Start: string | number | Date; StartTimezone: any; End: string | number | Date; EndTimezone: any; isAllDay: any; Title: any; Description: any; RecurrenceRule: any; RecurrenceID: any; RecurrenceException: any; }) => (
        {
            id: dataItem.TaskID,
            start: parseAdjust(dataItem.Start),
            startTimezone: dataItem.StartTimezone,
            end: parseAdjust(dataItem.End),
            endTimezone: dataItem.EndTimezone,
            isAllDay: dataItem.isAllDay,
            title: dataItem.Title,
            description: dataItem.Description,
            recurrenceRule: dataItem.RecurrenceRule,
            recurrenceId: dataItem.RecurrenceID,
            recurrenceExceptions: dataItem.RecurrenceException,
            roomId: randomInt(1, 2),
            personId: randomInt(1, 2)
        }
    ));
    
    const sampleDataWithCustomSchema = baseData.map((dataItem: { Start: string | number | Date; End: string | number | Date; }) => (
        {
            ...dataItem,
            Start: parseAdjust(dataItem.Start),
            End: parseAdjust(dataItem.End),
            PersonIDs: randomInt(1, 2),
            RoomID: randomInt(1, 2)
        }
    ));
    return (
        <Scheduler data={sampleData} defaultDate={displayDate}>
            <DayView showWorkHours={false} />
            <WeekView showWorkHours={false} currentTimeMarker={false} />
        </Scheduler>
    )
}

export default Calendar
