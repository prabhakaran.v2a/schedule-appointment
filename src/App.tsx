import React from 'react';
import logo from './logo.svg';
import './App.css';
import { StyleProvider } from '@ant-design/cssinjs';
import ListingComponent from './components/ListingComponent';
function App() {
  return (
    
    <StyleProvider>
      <div className="App">
        <ListingComponent/>
      </div>
      
    </StyleProvider>
  );
}

export default App;
